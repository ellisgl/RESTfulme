# RESTfulmé

RESTful API system for job postings and résumé uploading done in PHP >=8.1 (Symfony 6). It'll be best used as a way to filter out developer candidates.
